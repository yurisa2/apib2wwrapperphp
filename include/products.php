<?php
class products extends b2w_rest
{
  public function get_products()
  {
    $return = $this->b2w->get("products");

    return $return;
  }
  public function post_product($params)
  {
    $return = $this->b2w_post->post("products",$params);

    return $return;
  }

  public function get_single_product($sku)
  {
    $return = $this->b2w_put->get("products/".$sku);

    return $return;
  }

  public function get_products_list()
  {
    $product_list = json_decode($this->get_products()->response);
    foreach ($product_list->products as $key => $value) $product_skus[] = $value->sku;

    return $product_skus;
  }

  public function update_product($sku,$product_data)
  {
    $product = array('product' => $product_data);

    $return = $this->b2w_post->put("products/$sku",json_encode($product));

    return $return;
  }

}
?>
