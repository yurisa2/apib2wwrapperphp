<?php
class b2w_rest
{
  public function __construct()
  {
    $this->b2w_put = new RestClient([
        'base_url' => "https://api.skyhub.com.br",
        'headers' => [
          'x-accountmanager-key' => ACCMANAGER,
          'x-user-email' => EMAIL ,
          'x-api-key' => API_KEY]]);

    $this->b2w = new RestClient([
        'base_url' => "https://api.skyhub.com.br",
        'format' => "json",
        'headers' => [
          'x-accountmanager-key' => ACCMANAGER,
          'x-user-email' => EMAIL ,
          'x-api-key' => API_KEY]]);

    $this->b2w_post = new RestClient([
        'base_url' => "https://api.skyhub.com.br",
        // 'format' => "json",
        'headers' => [
          'accept' => 'application/json',
          'content-type' => 'application/json',
          'x-accountmanager-key' => ACCMANAGER,
          'x-user-email' => EMAIL ,
          'x-api-key' => API_KEY]]);


    $this->b2w_label = new RestClient([
        'base_url' => "https://api.skyhub.com.br",
        'headers' => [
          'accept' => 'application/pdf',
          'content-type' => 'application/json',
          'x-accountmanager-key' => ACCMANAGER,
          'x-user-email' => EMAIL ,
          'x-api-key' => API_KEY]]);

  }
}
 ?>
