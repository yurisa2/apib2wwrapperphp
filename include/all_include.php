<?php
if(!isset($prefix_b2w)) $prefix_b2w = '';

require_once $prefix_b2w.'include/restclient/vendor/autoload.php';
require_once $prefix_b2w.'include/skyhub/vendor/autoload.php';

require_once $prefix_b2w.'include/config.php';
require_once $prefix_b2w.'include/defines.php';

require_once $prefix_b2w.'include/b2w_rest.php';
require_once $prefix_b2w.'include/products.php';
require_once $prefix_b2w.'include/orders.php';

 ?>
