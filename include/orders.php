<?php
class order extends b2w_rest
{
  public function __construct()
  {
    parent::__construct();
  }

  public function get_orders()
  {
    // $param = array('filters' => array('statuses' => $status));
    $orders = $this->b2w_put->get('/orders');

    return $orders;
  }

  public function get_order_list()
  {
    $order_list = json_decode($this->get_orders()->response);

    foreach ($order_list->orders as $key => $value) {
      if($value->status->type == "APPROVED") {
        $order_ids[] = array('updated_at' => $value->updated_at,'code' => $value->code);
      }
    }

    if(!isset($order_ids)) return false;

    sort($order_ids);
    foreach ($order_ids as $key => $value) $list_ids[] = $value['code'];

    return $list_ids;
  }

  public function get_order_information($id_order)
  {
    $data_order = $this->b2w_put->get("/orders/$id_order");

    return $data_order;
  }

  public function b2wGetOrdersPlp()
  {
    $label = $this->b2w_label->get("/shipments/b2w/to_group");

    return $label;
  }

  public function b2wGetGroupedPlp()
  {
    $label = $this->b2w_label->get("/shipments/b2w");

    return $label;
  }

  /* $order_ids  array*/
  public function b2wPostOrdersPlp($order_ids)
  {
    $body = ['order_remote_codes' => $order_ids];
    $label = $this->b2w_put->post("/shipments/b2w",$body);

    return $label->response;
  }

  public function b2wGetOrderPlp($plpId)
  {
    $label = $this->b2w_label->get("/shipments/b2w/view?plp_id=$plpId");

    return $label->response;
  }

  public function add_order_envoice($order_id, $invoice_key)
  {
    $params = array(
      //"status" => "payment_received",
      "invoice" => array(
        "key" => $invoice_key
    ));
    $return = $this->b2w_put->post("/orders/$order_id/invoice", $params);

    return $return;
  }
}
?>
